# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH="/home/azazel/.oh-my-zsh"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="classyTouch"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"

plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# User configuration

export PATH="$HOME/.local/bin:$HOME/.npm-global/bin:$PATH"

# aliases
alias scaur='python ~/.scaur/scaur.py'
alias nf='neofetch --ascii ~/scripts/customnf.txt'
alias ss='~/scripts/rofimgur.sh'
alias update='~/scripts/update'
alias v='nvim'
alias discline='~/scripts/discline'
alias cava='~/.programs/cava/cava'
