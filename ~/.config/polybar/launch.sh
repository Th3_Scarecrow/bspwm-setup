#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch
polybar bar &
polybar barws &
polybar barmenu &
polybar bardate &
polybar barmenu2 &
polybar ubar &

echo "Bar launched..."
