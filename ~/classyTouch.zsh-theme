# ZSH Theme - classyTouch
# Author: Yaris Alex Gutierrez <yarisgutierrez@gmail.com>
# Prompt is the Oh-my-zsh version of user Graawr's 'Classy Touch' themes on http://dotshare.it

local current_dir='$fg[red][$fg[white]%c$fg[red]]'
local git_branch='$(git_prompt_info)%{$reset_color%}'


PROMPT="%(?,%{$fg[magenta]%}${current_dir}%{$reset_color%} ${git_branch}
%{$fg[white]%}>>>%{$reset_color%} ,%{$fg[white]%}${current_dir}%{$reset_color%} ${git_branch}
%{$fg[white]%}>>>%{$reset_color%} "

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[magenta]%}["
ZSH_THEME_GIT_PROMPT_SUFFIX="] %{$reset_color%}"
