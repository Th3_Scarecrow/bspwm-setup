This is a git repo to exactly make my bspwm rice. 

First and foremost, let me just thank you for considering to use my rice.

For this ricing guide, I'll be using Arch Linux, but it can be done on most distros. 
- Our Window Manager: Bspwm, is a tiling window manager, that is very customizable and simple for begginers.
- Our Bar: Polybar, which works very well with bspwm and is a must have for a rice like this.
- Our file manager: Ranger, which is a very powerful, but lightweight file manager, with vim-like keybindings.
- My Terminal: I'll be using rxvt-unicode, because of its extreme customization options.
- Colorscheme: I'll provide you with my colorscheme in my Xresources file, but I recomend a program called pywal which generates a colorscheme off of your wallpaper.
- Required Fonts:
    - Profont: For Terminal and bar uses
    - Overpass: For Bar uses
    - Siji: For bar uses
    - Font Awesome 5: For ricing the shit out of polybar
- Also, You need rofi for making your ricing even better.

Let's start!

I'm assuming that you've installed a linux distro on your system. We require git for installing bspwm.
- `sudo pacman -S git` for Arch users
- `sudo apt-get install git` for Debian/Ubuntu/Linux Mint users

Once you done that, go ahead and clone baskerville's bspwm and sxhkd git repos.
 - `git clone https://github.com/baskerville/bspwm.git`
 - `git clone https://github.com/baskerville/sxhkd.git`

Some insight: SXHKD is a "Simple X HotKey Daemon" created by the same person who created bspwm. Its used for creating and changing keybinds and works well with bspwm.

Once you've got that cloned, we need a program called make, which is usually installed by default in most distros.
If you haven't got it installed, run

`sudo apt-get install make gcc` (Arch has it by default)

Then, cd into the directory where you cloned bspwm and run:

`make`
`sudo make install`
Do the same for the sxhkd repo. 

Great! You've got bspwm and sxhkd installed. 
Now, to save some time, you can clone my repo and proceed, or you can create your own config, which I can respect.

Let's move on.

Copy `bspwm/examples/bspwmrc` to `~/.config/bspwm/bspwmrc` and run `chmod +x ~/.config/bspwm/bspwmrc`

If you don't use a display manager (like me) then just add
`exec bspwm`
to the end of your .xinitrc file.


If you use a display manager (like Lightdm, kdm, gdm or mdm (this means you Ubuntu folks)) you'll probably want to copy the file provided in contrib/freedesktop to its standard locations ie copy bspwm.desktop to /usr/share/xsessions/. Attention: This replaces the loading via the ~/.xinitrc, don't use both.

On Fedora/Ubuntu/Mint/Others:

`sudo cp contrib/freedesktop/bspwm.desktop /usr/share/xsessions/`

You'll now need to restart your display manager or simply reboot your machine.

`sudo service [lightdm|kdm|gdm|mdm] restart`


Logging into your new bspwm session

Now for the moment of truth. Type startx in your tty. If all went according to plan, you should be presented with a blank screen. That's good. Open up your preferred terminal using whatever key combination / terminal name you chose in your .config/sxhkdrc. If nothing happens, start trouble shooting.

For Fedora 22: When you log back on, click the "gear" icon and select 'bspwm'

The Ricing begins...

You must've cloned this repo before proceeding.

First, let's install the necessary fonts and programs.

`sudo pacman -S ranger rxvt-unicode ttf-font-awesome profont`

Manually install the package `otf-overpass` or use an AUR helper to get the Overpass font.

If you have an .Xresources file, then back it up, delete it and copy mine, IF you want your setup to be the same as mine.

`cd /path/to/bspwm-rice && cp .Xresources ~/.Xresources`

Do the same with the other files, including the bspwmrc file.
Great. Now, let's set up your wallpaper. To do this, we need to install a program called feh
`sudo pacman -S feh`
Choose a wallpaper of your choice, or you can choose mine instead.
Open your bspwmrc file and there should be this line located near the top:
`feh --bg-scale /path/to/wallpaper`
Change it to the path to your wallpaper
Restart bspwm and your wallpaper should appear! Along with some borders, gaps and such.
The next step, which should be easy, consdiring I've taken out the hard part, setting up polybar

Setting up Polybar

Clone the polybar repo, or install it via the AUR. 
Then, just copy the contents inside the polybar folder to your `~/.config/polybar/` directory. 

That's it!

